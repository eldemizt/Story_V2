<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
    <title>${TITLE}</title>
    <style>
        body {
            margin: auto;
            width: 100vw;
            height: 100vh;
            display: flex;
            flex-flow: row wrap;
            align-content: flex-start;
            background-color: #252729;
        }

        #header {
            color: white;
            text-align: left;
            padding: 5px;
            width: 100vw;
        }

        #link {
            line-height: 30px;
            background-color: #66686a;
            width: 20%;
            height: 93vh;
        }

        #content {
            background-color: #a8aaac;
            width: 80%;
            height: 93vh;

        }

        #footer {
            color: white;
            text-align: left;
            padding: 5px;
            width: 100vw;
        }
    </style>
</head>
<body>
<div id="header">Welcome ${USER} ${EMAIL}</div>
<div id="link" style="color: black">
    <button id="prev">Previous page</button>
    <button id="next">Next page</button>
</div>
<div id="content" style="color: black">
    <div id="divs">
    <#list PAGE as PAGES>
        <p>${PAGES}</p>
        <#else>
        <p>Story Doesn't Exist.</p>
    </#list>
    </div>
</div>
<div id="footer">Zach Eldemire Reader</div>

<script>
    $(document).ready(function(){
						//Initially hides all divs where the page will go
        $("#divs p").each(function(e) {
            if (e != 0)
                $(this).hide();
        });
        var len = $("#divs p").length;
        var count = 1;
        $('#prev').hide();
	if(len == 1)				//If there is only 1 page then dont show any buttons
            $('#next').hide();
        $("#next").click(function(){
            count++;
            if (count == len)				//test to see if this is the last page
                $('#next').hide();			//if it is hide the next button
            if ($("#divs p:visible").next().length != 0)
                $("#divs p:visible").next().show().prev().hide();

            else {
                $("#divs p:visible").hide();
                $("#divs p:first").show();

            }
            $('#prev').show();				//if the next button is clicked the prev button will show also
            return false;
        });

        $("#prev").click(function(){
            $('#next').show();				//if the prev button is clicked the next button will show also
            count--;
            if(count == 1)				//test to see if the first page is showing
                $('#prev').hide();			//if it is hide the prev button
            if ($("#divs p:visible").prev().length != 0)
                $("#divs p:visible").prev().show().next().hide();
            else {
                $("#divs p:visible").hide();
                $("#divs p:last").show();
            }
            return false;
        });
    });
</script>
</body>
</html>
